//Rachel Hegeman and Rosellen Grant
//rachel.hegeman@gmail.com, rgrant8@jhu.edu

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Cache simulator main method.
 */
public final class CacheSimulator {

    private static final int SIXTEEN = 16;
    private static final int THREE = 3;
    private static final int FOUR = 4;
    private static final int FIVE = 5;
    private static final int SIX = 6;
    private static final int SEVEN = 7;

    /** Default constructor. */
    private CacheSimulator() { }

    /**
     * Main method.
     * @param args command line arguments.
     */
    public static void main(String[] args) {

        if (checkArgs(args) == 1) {
            System.out.println("Error: Invalid input.");
            return;
        }

        long numS, numBl, numBy;
        int wa, wt, e;
        try {
            numS = Long.parseLong(args[0]);
            numBl = Long.parseLong(args[1]);
            numBy = Long.parseLong(args[2]);
            wa = Integer.parseInt(args[THREE]);
            wt = Integer.parseInt(args[FOUR]);
            e = Integer.parseInt(args[FIVE]);
        } catch (NumberFormatException err1) {
            System.out.println("Error: Invalid input.");
            return;
        }

        if (checkVars(numS, numBl, numBy, wa, wt, e) == 1) {
            System.out.println("Error: Invalid input.");
            return;
        }

        File file = new File(args[SIX]);
        Scanner fileReader;
        try {
            fileReader = new Scanner(file);
        } catch (FileNotFoundException err2) {
            System.out.println("Error: Invalid input.");
            return;
        }

        //Generate cache on command line args
        Cache theCache = new Cache(numS, numBl, numBy, wa, wt, e);

        while (fileReader.hasNext()) {

            //Read through file line by line
            String line = fileReader.nextLine();
            String[] lineParse = line.split("\\s+");
            long addr = Long.parseLong(lineParse[1].substring(2), SIXTEEN);

            if (lineParse[0].toLowerCase().equals("l")) {
                theCache.load(addr);
            } else {
                theCache.store(addr);
            }
        }

        //Print summary to standard out
        System.out.printf("Total loads: %d\n"
                + "Total stores: %d\n"
                + "Load hits: %d\n"
                + "Load misses: %d\n"
                + "Store hits: %d\n"
                + "Store misses %d\n"
                + "Total cycles: %d\n",
                theCache.getTotalLoads(),
                theCache.getTotalStores(),
                theCache.getLoadHits(),
                theCache.getLoadMisses(),
                theCache.getStoreHits(),
                theCache.getStoreMisses(),
                theCache.getCycles());

        fileReader.close();
    }

    /**
     * Check variables for validity.
     * @return 1 if bad variables, 0 if good
     */
    private static int checkVars(long numS, long numBl,
            long numBy, int wa, int wt, int e) {
        if (wa == 0 && wt == 0) { //no write allocate and write back
            return 1;
        }

        //check powers of 2. numS, numBl and numBy
        if (numS < 0 || numBl < 0) {
            return 1;
        }
        if (numBy < FOUR) {
            return 1;
        }
        if (checkPowerOfTwo(numS, numBl, numBy) == 1) {
            return 1;
        }

        // wa, wt, e all 0 or 1
        if (wa >> 1 != 0 || wt >> 1 != 0 || e >> 1 != 0) {
            return 1;
        }

        return 0;
    }

    /**
     * Check variables that should be power of 2.
     * @param l1 variable 1
     * @param l2 variable 2
     * @param l3 variable 3
     * @return 1 if invalid, 0 if valid
     */
    private static int checkPowerOfTwo(long l1, long l2, long l3) {
        if ((l1 & -l1) != l1) {
            return 1;
        }
        if ((l2 & -l2) != l2) {
            return 1;
        }
        if ((l3 & -l3) != l3) {
            return 1;
        }
        return 0;
    }

    /**
     * Check command line args.
     * @return 1 if bad input, 0 if good
     */
    private static int checkArgs(String[] args) {
        if (args.length != SEVEN) {
            return 1;
        }
        return 0;
    }
}