//Rachel Hegeman and Rosellen Grant
//rachel.hegeman@gmail.com, rgrant8@jhu.edu
/**
 * Node class for linked list in hashmap cache structure.
 * @author RosellenGrant
 *
 */
public class Node {
    private long tag;
    private int dirtyBit; //0 or 1

    /**
     * Default constructor.
     */
    public Node() { }

    /**
     * Create a node with default clean dirty bit.
     * @param t Node's tag
     */
    public Node(long t) {
        this.tag = t;
        this.dirtyBit = 0; //clean
    }

    /**
     * @return the tag
     */
    public long getTag() {
        return this.tag;
    }

    /**
     * @param t the tag to set
     */
    public void setTag(long t) {
        this.tag = t;
    }

    /**
     * @return the dirtyBit
     */
    public int getDirtyBit() {
        return this.dirtyBit;
    }

    /**
     * @param db the dirtyBit to set
     */
    public void setDirtyBit(int db) {
        this.dirtyBit = db;
    }

    @Override
    public boolean equals(Object other) {
        if (other != null && other instanceof Node) {
            Node v = (Node) other;
            return (this.tag == v.getTag());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return this.tag + "";

    }

}
