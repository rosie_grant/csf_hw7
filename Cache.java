//Rachel Hegeman and Rosellen Grant
//rachel.hegeman@gmail.com, rgrant8@jhu.edu

import java.util.Hashtable;
import java.util.LinkedList;
/**
 * Cache class to hold data structure of cache (hashmap
 * with linked lists of blocks).
 */
public class Cache {
    private static final int HUNDRED = 100;
    private static final int FOUR = 4;
    private Hashtable<Long, LinkedList<Node>> laCache;

    private long numSets; //number of sets in the cache, for indexing
    private long numBlocks; //number of blocks in each set
    private long numBytes; //number of bytes in each block
    private int writeAlloc; //1 if write allocate, 0 if no write allocate
    private int writeThrough; //1 if write through, 0 if write back
    private int evic; // 1 if LRU, 0 if FIFO

    private long totalLoads;
    private long totalStores;
    private long loadHits;
    private long loadMisses;
    private long storeHits;
    private long storeMisses;
    private long cycles;


    /**
     * Default constructor.
     */
    public Cache() { }

    /**
     * @param numS number of sets
     * @param numBl length of linked lists in the cache
     * @param numBy number of bytes/block
     * @param wa write alloc option
     * @param wt write through option
     * @param e eviction option
     */
    public Cache(long numS, long numBl, long numBy, int wa, int wt, int e) {
        this.laCache = new Hashtable<Long, LinkedList<Node>>(); //init
        this.numSets = numS;
        this.numBlocks = numBl;
        this.numBytes = numBy;
        this.writeAlloc = wa;
        this.writeThrough = wt;
        this.evic = e;
    }

    /**
     * Load into the CPU. If not in cache,
     * get from memory.
     * @param address the address of the block to load
     */
    public void load(long address) {
        this.totalLoads++;

        // need to account for block size in address
        long mask = (this.numSets - 1)
                << (long) (Math.log(this.numBytes) / Math.log(2));
        long i = mask & address;
        long tag = (~mask - this.numBytes + 1) & address; //flip bits

        //check cache at index for this block
        Node temp = new Node(tag); //**changed this from tag
        LinkedList<Node> aList = this.laCache.get(i);

        if (aList == null) {
            aList = new LinkedList<Node>(); //empty list
        }

        if (!aList.contains(temp)) { //cache doesn't have block, load miss
            // add a block with this address
            this.addBlock(i, aList, temp);
            this.loadMisses++;
            this.cycles += this.numBytes / FOUR * HUNDRED;
            //loading from RAM to cache
            // this.cycles += 1; //loading from cache to CPU
            // maybe don't have to do this.. since were already reading from RAM
        } else { //cache has the block, this was a load hit
            //update the queue if necessary
            this.updateQueue(i, aList, tag);
            this.loadHits++;
            this.cycles += 1; //loading from the cache to CPU

        }

    }

    /**
     * Update the queue structure at each key in the hashmap,
     * only if the eviction status is LRU. Moves the
     * Node with passed tag to the back of the queue, indicating
     * that it was most recently used. If eviction status is FIFO,
     * this method accomplishes nothing.
     * @param index
     * @param aList
     * @param tag
     */
    private void updateQueue(long index, LinkedList<Node> aList, long tag) {
        if (this.evic == 1) {
            int j = 0;
            while (aList.get(j).getTag() != tag) {
                j++;
            }
            Node theNode = aList.remove(j); //remove the node
            aList.addLast(theNode); // enqueue, this node is now the most
            //recently accessed
            this.laCache.put(index, aList); //will overwrite what's at index,
            //which is the old version of this list
        }
    }

    /**
     * Add a block to the linked list queue.
     * If it is necessary to remove ("evict") a node, checks if we're
     * in write back, and if the evicted block has a dirty bit.
     * If both are true, we write the block to memory, adding to cycle
     * count.
     *
     * Note: when we add this new block, if we're in write back,
     * this method does not account for the cycles of writing the
     * new block to the cache.
     * @param temp
     */
    private void addBlock(long i, LinkedList<Node> aList, Node temp) {

        Node removed = null;
        //check the size of the queue
        if (aList.size() >= this.numBlocks) { //queue is full
            //dequeue the front, will be the least recently used if LRU,
            //or the node in the queue the longest if FIFO
            removed = aList.removeFirst();
        }
        //enqueue new node (at the back)
        aList.add(temp);
        //put back in the hashmap
        this.laCache.put(i, aList);

        //Now, if removed had a dirty bit and we're doing write back,
        //need to write the removed block to memory
        if (this.writeThrough == 0 && removed != null
                && removed.getDirtyBit() == 1) {
            this.cycles += this.numBytes / FOUR * HUNDRED;  //writing to RAM
        }

    }
    /**
     * Write to the cache and/or memory.
     * @param address address of memory to which to write
     */
    public void store(long address) {

        this.totalStores++;

        // need to account for block size in address
        long mask = (this.numSets - 1)
                << (long) (Math.log(this.numBytes) / Math.log(2));
        long i = mask & address;
        long tag = (~mask - this.numBytes + 1) & address; //flip bits

        Node temp = new Node(tag);
        LinkedList<Node> aList = this.laCache.get(i);
        if (aList == null) {
            aList = new LinkedList<Node>(); //empty list
        }


        if (aList.contains(temp)) { // if the cache has a block with this tag

            this.storeHits++;
            this.cycles += 1; // accounts for cache write

            if (this.writeThrough == 1) { // if it's also write through
                this.cycles += HUNDRED; // accounts for RAM write
            } else { // else if it's write back
                int j = aList.indexOf(temp);
                Node n = aList.remove(j); //take it out of the list
                n.setDirtyBit(1); //set dirty bit
                aList.add(j, n);  //put back
            }
            this.updateQueue(i, aList, tag); //update queue, replace it in cache

        } else { // if the cache doesn't contain the block, write miss

            this.storeMisses++;
            if (this.writeAlloc == 1 && this.writeThrough == 1) {
                //write allocate
                //write 1 word to memory
                this.cycles += HUNDRED;
                //get updated block from memory and store in cache
                this.cycles += this.numBytes / FOUR * HUNDRED;
                this.addBlock(i, aList, temp); // add to cache data struct
                // ^^ may need to update, not sure if we write to ram first
            } else if (this.writeAlloc == 1 && this.writeThrough == 0) {
                this.cycles += this.numBytes / FOUR * HUNDRED; //RAM read
                this.cycles += this.numBytes / FOUR; // cache write
                // this.cycles += 1; // account for word write in cache
                temp.setDirtyBit(1); //set dirty bit
                this.addBlock(i, aList, temp); // add to cache data struct
            } else {
                //means this.writeAlloc == 0 && this.writeThrough == 1)
                //don't get block from memory, don't write to cache
                //write just to memory directly
                this.cycles += HUNDRED;
            }
        }
    }

    /**
     * Get total loads.
     * @return the totalLoads
     */
    public long getTotalLoads() {
        return this.totalLoads;
    }

    /**
     * Get total stores.
     * @return the totalStores
     */
    public long getTotalStores() {
        return this.totalStores;
    }

    /**
     * Get load hits.
     * @return the loadHits
     */
    public long getLoadHits() {
        return this.loadHits;
    }

    /**
     * Get store hits.
     * @return the storeHits
     */
    public long getStoreHits() {
        return this.storeHits;
    }

    /**
     * Get load misses.
     * @return the loadMisses
     */
    public long getLoadMisses() {
        return this.loadMisses;
    }

    /**
     * Get store misses.
     * @return the storeMisses
     */
    public long getStoreMisses() {
        return this.storeMisses;
    }

    /**
     * Get cycles.
     * @return the cycles
     */
    public long getCycles() {
        return this.cycles;
    }

}
